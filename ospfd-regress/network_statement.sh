#!/bin/ksh
#	$OpenBSD: network_statement.sh,v 1.3 2020/06/12 13:27:43 denis Exp $
set -e

OSPFD=$1
OSPFDCONFIGDIR=$2
RDOMAIN1=$3
RDOMAIN2=$4
PAIR1=$5
PAIR2=$6

RDOMAINS="${RDOMAIN1} ${RDOMAIN2}"
PAIRS="${PAIR1} ${PAIR2}"
PAIR1IP=192.168.99.${RDOMAIN1}
PAIR2IP=192.168.99.${RDOMAIN2}
PAIR1PREFIX=10.${RDOMAIN1}.0
PAIR2PREFIX=10.${RDOMAIN2}.0
PAIR2PREFIX2=10.${RDOMAIN2}.${RDOMAIN2}

error_notify() {
	echo cleanup
	pkill -T ${RDOMAIN1} ospfd || true
	pkill -T ${RDOMAIN2} ospfd || true
	sleep 1
	ifconfig ${PAIR2} destroy || true
	ifconfig ${PAIR1} destroy || true
	ifconfig vether${RDOMAIN1} destroy || true
	ifconfig vether${RDOMAIN2} destroy || true
	route -qn -T ${RDOMAIN1} flush || true
	route -qn -T ${RDOMAIN2} flush || true
	ifconfig lo${RDOMAIN1} destroy || true
	ifconfig lo${RDOMAIN2} destroy || true
	rm ${OSPFDCONFIGDIR}/ospfd.1.conf ${OSPFDCONFIGDIR}/ospfd.2.conf
	if [ $1 -ne 0 ]; then
		echo FAILED
		exit 1
	else
		echo SUCCESS
	fi
}

if [ "$(id -u)" -ne 0 ]; then
	echo need root privileges >&2
	exit 1
fi

trap 'error_notify $?' EXIT

echo check if rdomains are busy
for n in ${RDOMAINS}; do
	if /sbin/ifconfig | grep -v "^lo${n}:" | grep " rdomain ${n} "; then
		echo routing domain ${n} is already used >&2
		exit 1
	fi
done

echo check if interfaces are busy
for n in ${PAIRS}; do
	/sbin/ifconfig "${n}" >/dev/null 2>&1 && \
	    ( echo interface ${n} is already used >&2; exit 1 )
done

set -x

echo setup
ifconfig ${PAIR1} rdomain ${RDOMAIN1} ${PAIR1IP}/24 up
ifconfig ${PAIR2} rdomain ${RDOMAIN2} ${PAIR2IP}/24 up
ifconfig ${PAIR1} patch ${PAIR2}
ifconfig lo${RDOMAIN1} 10.0.0.${RDOMAIN1}/32
ifconfig lo${RDOMAIN2} 10.0.0.${RDOMAIN2}/32
ifconfig vether${RDOMAIN1} rdomain ${RDOMAIN1} ${PAIR1PREFIX}/24 up
ifconfig vether${RDOMAIN2} rdomain ${RDOMAIN2} ${PAIR2PREFIX}/24 up
ifconfig vether${RDOMAIN2} rdomain ${RDOMAIN2} ${PAIR2PREFIX2}/30 alias
sed "s/{RDOMAIN1}/${RDOMAIN1}/g;s/{PAIR1}/${PAIR1}/g" \
    ${OSPFDCONFIGDIR}/ospfd.network_statement.rdomain1.conf \
    > ${OSPFDCONFIGDIR}/ospfd.1.conf
chmod 0600 ${OSPFDCONFIGDIR}/ospfd.1.conf
sed "s/{RDOMAIN2}/${RDOMAIN2}/g;s/{PAIR2}/${PAIR2}/g" \
    ${OSPFDCONFIGDIR}/ospfd.network_statement.rdomain2.conf \
    > ${OSPFDCONFIGDIR}/ospfd.2.conf
chmod 0600 ${OSPFDCONFIGDIR}/ospfd.2.conf

echo add routes
route -T ${RDOMAIN2} add default ${PAIR2PREFIX}.1
route -T ${RDOMAIN2} add 172.16.0.0/24 ${PAIR2PREFIX}.2
route -T ${RDOMAIN2} add 172.17.0.0/22 ${PAIR2PREFIX}.3 -label toOSPF
route -T ${RDOMAIN1} add 172.18.0.0/28 ${PAIR1PREFIX}.254 -priority ${RDOMAIN1}

echo start ospf6d
route -T ${RDOMAIN1} exec ${OSPFD} \
    -v -f ${OSPFDCONFIGDIR}/ospfd.1.conf
route -T ${RDOMAIN2} exec ${OSPFD} \
    -v -f ${OSPFDCONFIGDIR}/ospfd.2.conf

sleep 50

echo tests
route -T ${RDOMAIN1} exec ospfctl sh fib
route -T ${RDOMAIN1} exec ospfctl sh rib | \
    grep "10.0.0.${RDOMAIN2}"
route -T ${RDOMAIN1} exec ospfctl sh rib | \
    grep "${PAIR2PREFIX}.0/24"
route -T ${RDOMAIN1} exec ospfctl sh rib | \
    grep "${PAIR2PREFIX2}.0/30"
route -T ${RDOMAIN1} exec ospfctl sh rib | \
    grep "172.16.0.0/24"
route -T ${RDOMAIN1} exec ospfctl sh rib | \
    grep "0.0.0.0/0"
route -T ${RDOMAIN1} exec ospfctl sh rib | \
    grep "172.17.0.0/22"
route -T ${RDOMAIN2} exec ospfctl sh rib | \
    grep "172.18.0.0/28"

exit 0
