# Peertube v2 installation in OpenBSD

(based on the amazing work from https://www.22decembre.eu/fr/2019/12/09/peertube-14-openbsd/)

### Install OpenBSD
Install a stock OpenBSD.

### Install dependencies
Install required packages
> pkg_add ffmpeg redis postgresql-server postgresql-contrib node nginx unzip--iconv git glib2 libwebp vips

Install yarn
> npm install --global yarn

### Configure dependencies
``youtube-dl`` installed by ``node.js`` needs to find ``python``
> ln -sf /usr/local/bin/python3 /usr/local/bin/python

``sharp`` is looking for a specific ``node.js`` version number :sigh:
> ln -sf /usr/local/share/node/node-v12.13.1.tar.gz /usr/local/share/node/node-v12.9.0.tar.gz

### Create a user
Peertube will be launched with user ``_peertube``
> useradd -m -d /var/www/peertube _peertube

Modify ``/etc/login.conf`` to change user profile
>  _peertube: \\ \
		:openfiles=1024: \\ \
		:tc=daemon:

### Configure PostgreSQL
As ``_postgresql`` user
> su - _postgresql

Initialize PostgreSQL storage
> mkdir /var/postgresql/data

> initdb -D /var/postgresql/data -U _postgresql -A scram-sha-256 -E UTF8 -W

> pg_ctl -D /var/postgresql/data -l logfile start

Create and configure Peertube database
> createuser -P peertube

> createdb -O peertube peertube_prod

> psql -c "CREATE EXTENSION pg_trgm;" peertube_prod

> psql -c "CREATE EXTENSION unaccent;" peertube_prod

### Install Peertube
As ``_peertube`` user
> su - _peertube

Download and unpack Peertube
> mkdir config storage versions

> export VERSION=v2.0.0

> cd /var/www/peertube/versions

> ftp "https://github.com/Chocobozzz/PeerTube/releases/download/${VERSION}/peertube-${VERSION}.zip"

> unzip peertube-${VERSION}.zip && rm peertube-${VERSION}.zip

> cd ~ && ln -s versions/peertube-${VERSION} ./peertube-latest

Install Sharp
> cd ~/peertube-latest

> export CPATH=/usr/local/include/glib-2.0/:/usr/local/lib/glib-2.0/include/

> SHARP_IGNORE_GLOBAL_LIBVIPS=1 npm add sharp

Install Peertube
> cd ~/peertube-latest

Some modules need to find node in /tmp... :sigh:
> ln -sf /usr/local/bin/node /tmp/node

> yarn install --production --pure-lockfile
