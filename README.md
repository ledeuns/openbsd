# OpenBSD
OpenBSD misc patches

## List
* **arpkeep.diff** = Add a sysctl knob `net.inet.ip.arpkeep` to set ARP timeout value.
* **auto_update.conf** = A sample file to auto_update OpenBSD