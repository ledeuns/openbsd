# How to build promscale_extension on OpenBSD

[promsscale_extension](https://github.com/timescale/promscale_extension) is a [PostgreSQL](https://www.postgresql.org/) extension to improve [Promscale](https://www.timescale.com/promscale/) performance.

### Dependencies

- gmake
- openssl
- postgresql-server
- rust
- unzip

### Build promscale_extension

Log with `_postgresql` user :
```
# su _postgresql
```

Install `cargo-pgx` :
```
$ OPENSSL_LIB_DIR=/usr/local/lib/eopenssl OPENSSL_INCLUDE_DIR=/usr/local/include/eopenssl cargo install cargo-pgx
```

Initialize `pgx` :
```
$ cargo pgx init --pg13=/usr/local/bin/pg_config
```

Start a build :
```
$ LIBCLANG_PATH=/usr/local/lib gmake all
```

It should fail on building `pgx-pg-sys-0.2.0`.

Edit `/var/postgresql/.cargo/registry/src/github.com-1ecc6299db9ec823/pgx-pg-sys-0.2.0/src/submodules/mod.rs` to add `target_os = "openbsd"` :
```
--- /var/postgresql/.cargo/registry/src/github.com-1ecc6299db9ec823/pgx-pg-sys-0.2.0/src/submodules/mod.rs.orig  Mon Jan  3 20:02:05 2022
+++ /var/postgresql/.cargo/registry/src/github.com-1ecc6299db9ec823/pgx-pg-sys-0.2.0/src/submodules/mod.rs       Mon Jan  3 20:01:15 2022
@@ -20,7 +20,7 @@
     ) -> std::os::raw::c_int;
 }
 
-#[cfg(any(target_os = "macos", target_os = "freebsd"))]
+#[cfg(any(target_os = "macos", target_os = "freebsd", target_os = "openbsd"))]
 extern "C" {
     pub(crate) fn sigsetjmp(
         env: *mut crate::sigjmp_buf,
```

Edit `/var/postgresql/.cargo/registry/src/github.com-1ecc6299db9ec823/pgx-pg-sys-0.2.0/build.rs` to replace `make` with `gmake` :
```
--- /var/postgresql/.cargo/registry/src/github.com-1ecc6299db9ec823/pgx-pg-sys-0.2.0/build.rs.orig      Mon Jan  3 19:08:05 2022
+++ /var/postgresql/.cargo/registry/src/github.com-1ecc6299db9ec823/pgx-pg-sys-0.2.0/build.rs   Mon Jan  3 19:07:44 2022
@@ -560,7 +560,7 @@
     }
 
     let rc = run_command(
-        Command::new("make")
+        Command::new("gmake")
             .arg("clean")
             .arg(&format!("libpgx-cshim-{}.a", major_version))
             .env("PG_TARGET_VERSION", format!("{}", major_version))
```

Cleanup previous build :
```
$ rm -rf /var/postgresql/promscale_extension-0.3.0/target/release/build/pgx*
```

Relaunch the build :
```
$ LIBCLANG_PATH=/usr/local/lib gmake all
```

Copy extension to `/usr/local/lib/postgresql` :
```
# cp /var/postgresql/promscale_extension-0.3.0/promscale.so /usr/local/lib/postgresql/promscale.so
# cp /var/postgresql/promscale_extension-0.3.0/sql/promscale--0.* /usr/local/share/postgresql/extension
# cp /var/postgresql/promscale_extension-0.3.0/promscale.control  /usr/local/share/postgresql/extension
```

PostgreSQL must load `libc++abi` to successfully load the extension :
```
$ LD_PRELOAD=libc++abi.so postgres -D ./data
```
